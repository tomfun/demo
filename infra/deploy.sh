#!/usr/bin/env bash

set -e
# Assume IMAGE_TAG is passed
# example IMAGE_TAG - "4826e4e1efe2dfe66899df0003e93aca5214f3f5"
# example IMAGE_TAG - "master"
IMAGE_TAG="${IMAGE_TAG:-master}"

# Assume PUBLIC_HTTP_HOST is passed
# example PUBLIC_HTTP_HOST - "some.demo_blog.tomfun.co"
# example PUBLIC_HTTP_HOST - "tomfun.co"
PUBLIC_HTTP_HOST="${PUBLIC_HTTP_HOST:-demo.tomfun.co}"
docker rm -f -v "$PUBLIC_HTTP_HOST" || echo "There is no running container"

docker run \
    --name "$PUBLIC_HTTP_HOST" \
    --hostname "$PUBLIC_HTTP_HOST" \
    --env VIRTUAL_HOST="$PUBLIC_HTTP_HOST" \
    --network nginx_default \
    -d \
    --restart always \
    --label co_tomfun_ssl_inner_name=tomfun.co \
    registry.gitlab.com/tomfun/demo:"$IMAGE_TAG"
