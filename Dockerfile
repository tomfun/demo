FROM node:lts-jessie

WORKDIR /home/node

RUN git clone https://github.com/tomfun/demo-canvas-audio.git \
    && cd demo-canvas-audio \
    && npm i \
    && FORCE_COLOR=1 npm run build \
    && mv public/ /

FROM nginx

COPY . /usr/share/nginx/html

COPY --from=0 /public /usr/share/nginx/html/canvas-audio
