(function () {
  'use strict';


  /**
   * @mutate
   * @param {CanvasRenderingContext2D} canvas
   * @param {Number} width
   * @param {Number} height
   */
  function autoLevels(canvas, width, height) {
    var ImageData = canvas.getImageData(0, 0, width, height);
    var min = 255;
    var max = 0;
    var ind;
    for (var i = 0; i < height * width; i++) {
      ind = i * 4;
      min = Math.min(min, ImageData.data[ind], ImageData.data[ind + 1], ImageData.data[ind + 2]);
      max = Math.max(max, ImageData.data[ind], ImageData.data[ind + 1], ImageData.data[ind + 2]);
    }
    var mul = 255 / (max - min);
    for (i = 0; i < height * width; i++) {
      ind = i * 4;
      ImageData.data[ind] = Math.floor((ImageData.data[ind] - min) * mul);
      ImageData.data[ind + 1] = Math.floor((ImageData.data[ind + 1] - min) * mul);
      ImageData.data[ind + 2] = Math.floor((ImageData.data[ind + 2] - min) * mul);
    }

    canvas.putImageData(ImageData, 0, 0);//put image data back
  }

  /**
   * @mutate
   * @param {CanvasRenderingContext2D} canvas
   * @param {Number} width
   * @param {Number} height
   */
  function grayScale(canvas, width, height) {
    var ImageData = canvas.getImageData(0, 0, width, height);
    var ind;
    for (var i = 0; i < height * width; i++) {
      ind = i * 4;
      var sum = Math.floor((ImageData.data[ind] + ImageData.data[ind + 1] + ImageData.data[ind + 2]) / 3);
      ImageData.data[ind++] = sum;
      ImageData.data[ind++] = sum;
      ImageData.data[ind++] = sum;
    }

    canvas.putImageData(ImageData, 0, 0);//put image data back
  }

  /**
   * @mutate
   * @param {CanvasRenderingContext2D} canvas
   * @param {Number} width
   * @param {Number} height
   * @param {Number} radius
   * @param {Number} factor
   */
  function sharpenVertial(canvas, width, height, radius, factor) {
    // todo
    var ImageData = canvas.getImageData(0, 0, width, height);
    var ind;
    for (var x = 0; x < width; x++) {
      for (var y = 0; y < height; y++) {
        ind = (y + x * height) * 4;
        var sum = Math.floor((ImageData.data[ind] + ImageData.data[ind + 1] + ImageData.data[ind + 2]) / 3);
        ImageData.data[ind++] = sum;
        ImageData.data[ind++] = sum;
        ImageData.data[ind++] = sum;
      }
    }
    canvas.putImageData(ImageData, 0, 0);//put image data back
  }

  /**
   * @mutate
   * @param {CanvasRenderingContext2D} canvas
   * @param {Number} width
   * @param {Number} height
   * @param {Number[][]} convM
   */
  function convolution(canvas, width, height, convM) {
    var imageData = canvas.getImageData(0, 0, width, height);
    var indOut;
    var indIn;
    var out = [];
    var x, y;
    for (x = 0; x < width; x++) {
      out[x] = out[x] || [];
      for (y = 0; y < height; y++) {
        indOut = (y + x * height) * 4;
        var v = [0, 0, 0];
        for (var i = 0; i < convM.length; i++) {
          for (var j = 0; j < convM[i].length; j++) {
            var ir = i - Math.floor(convM.length / 2);
            var jr = j - Math.floor(convM[i].length / 2);
            var ia = ir + x;
            var ja = jr + y;
            if (ia >= 0 && ia < width && ja >= 0 && ja < height) {
              indIn = (ja + ia * height) * 4;
              v[0] += imageData.data[indIn++] * convM[i][j];
              v[1] += imageData.data[indIn++] * convM[i][j];
              v[2] += imageData.data[indIn++] * convM[i][j];
            }
          }
        }
        out[x][y] = v;
      }
    }
    for (x = 0; x < width; x++) {
      out[x] = out[x] || [];
      for (y = 0; y < height; y++) {
        indOut = (y + x * height) * 4;
        imageData.data[indOut++] = out[x][y][0];
        imageData.data[indOut++] = out[x][y][1];
        imageData.data[indOut++] = out[x][y][2];
      }
    }
    canvas.putImageData(imageData, 0, 0);//put image data back
  }

  /**
   * @param {CanvasRenderingContext2D} ctx
   * @param {Number} width
   * @param {Number} height
   * @param {Number} memory
   * @constructor
   */
  function MotionBlur(ctx, width, height, memory) {
    this.ctx = ctx;
    this.width = width;
    this.height = height;
    this.memory = memory;
    this.init();
  }

  MotionBlur.prototype.init = function initMotionBlur() {
    this.pixels = [];
    var len = this.width * this.height;
    var ind;
    for (var i = 0; i < len; i++) {
      ind = i * 4;
      this.pixels[ind++] = 0;
      this.pixels[ind++] = 0;
      this.pixels[ind++] = 0;
      this.pixels[ind++] = 1;
    }
  };

  MotionBlur.prototype.process = function processMotionBlur() {
    var ImageData = this.ctx.getImageData(0, 0, this.width, this.height);
    var ind;
    for (var i = 0; i < this.height * this.width; i++) {
      for (var j = 0; j < 4; j++) {
        ind = i * 4 + j;
        this.pixels[ind] = (this.pixels[ind] * (this.memory - 1) + ImageData.data[ind]) / this.memory;
        ImageData.data[ind] = this.pixels[ind] & 255;
      }
    }

    this.ctx.putImageData(ImageData, 0, 0);//put image data back
  };

  window.convolution = convolution;
  window.autoLevels = autoLevels;
  window.grayScale = grayScale;
  window.MotionBlur = MotionBlur;
})();
