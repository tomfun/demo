(function () {
  'use strict';

  /**
   * @mutate
   * @param {CanvasRenderingContext2D} ctxIn
   * @param {Number} width
   * @param {Number} height
   * @param {CanvasRenderingContext2D} dCtx
   * @param {Number} dWidth
   * @param {Number} dHeight
   */
  function rombify(ctxIn, width, height, dCtx, dWidth, dHeight) {
    var inImageData = ctxIn.getImageData(0, 0, width, height);
    var shagX = dWidth / width;
    var shagY = dHeight / height;
    var sw = shagX / Math.sqrt(2);
    var sh = shagX / Math.sqrt(2);
    var ss = 0.5

    function pixelMatrix() {
      var matrix = [];
      var ind;
      for (var x = 0; x < width; x++) {
        matrix[x] = matrix[x] || [];
        for (var y = 0; y < height; y++) {
          ind = (x + (y * width)) * 4;
          var pixelNorm = (inImageData.data[ind] + inImageData.data[ind + 1] + inImageData.data[ind + 2]) / (3 * 255);
          matrix[x][y] = pixelNorm;
        }
      }

      return matrix;
    }

    function helpLines() {
      dCtx.beginPath();
      for (var i = 0; i < dWidth; i += 10) {
        dCtx.moveTo(i, 0);
        dCtx.lineTo(i, dHeight);
      }
      dCtx.stroke();
      dCtx.beginPath();
      for (var i = 0; i < dHeight; i += 10) {
        dCtx.moveTo(0, i);
        dCtx.lineTo(dWidth, i);
      }
      dCtx.stroke();
    }

    function initCanvas() {
      dCtx.fillStyle = 'rgba(0, 0, 0, 0.7)';
      dCtx.strokeStyle = 'rgba(255, 255, 255, 1)';
      dCtx.fillRect(0, 0, dWidth, dHeight);
      dCtx.fillStyle = 'rgba(255, 255, 255, 1)';
    }

    function drawPixel(p, x, y) {
      dCtx.save();
      dCtx.transform(1, 0, 0, 1, x * shagX - sw * ss / 2, y * shagY - sh * ss / 2);
      dCtx.rotate(Math.PI / 4);
      var c = 0.5 + p / 2;
      dCtx.transform(c, 1 - c, 1 - c, c, 0, 0);
      // dCtx.transform(1, 0, 0, 1, 0, 0);
      dCtx.fillRect(sw, sh, sw * ss, sh * ss);
      dCtx.restore();
    }

    function draw(matrix) {
      for (var x = 0; x < width; x++) {
        for (var y = 0; y < height; y++) {
          drawPixel(matrix[x][y], x, y);
        }
      }
    }

    initCanvas();
    draw(pixelMatrix());
  }

  window.rombify = rombify;
})();
