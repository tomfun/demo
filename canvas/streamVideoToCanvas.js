// Polyfill
if (navigator.mediaDevices === undefined) {
  navigator.mediaDevices = {};
}
if (navigator.mediaDevices.getUserMedia === undefined) {
  navigator.mediaDevices.getUserMedia = function (constraints) {

    // First get ahold of the legacy getUserMedia, if present
    var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    // Some browsers just don't implement it - return a rejected promise with an error
    // to keep a consistent interface
    if (!getUserMedia) {
      return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
    }

    // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
    return new Promise(function (resolve, reject) {
      getUserMedia.call(navigator, constraints, resolve, reject);
    });
  }
}

function streamVideoToCanvas(canvas) {
  return navigator.mediaDevices.getUserMedia({
    audio: false,
    video: {width: 640, height: 480}
  })
    .then((stream) => {
      if ('srcObject' in canvas) {
        canvas.srcObject = stream;
      } else {
        canvas.src = window.URL.createObjectURL(stream);
      }
      return new Promise((res) => {
        canvas.onloadedmetadata = function (event) {
          canvas.play();
          res(event);
        };
      })
    })
    .catch((err) => {
      const msg = "The following error occurred: " + err.name + ', ' + err.message;
      console.log(msg, err);
      alert(msg);
    });
}