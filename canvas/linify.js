(function () {
  'use strict';

  /**
   * @mutate
   * @param {CanvasRenderingContext2D} ctxIn
   * @param {Number} width
   * @param {Number} height
   * @param {CanvasRenderingContext2D} dCtx
   * @param {Number} dWidth
   * @param {Number} dHeight
   */
  function linify(ctxIn, width, height, dCtx, dWidth, dHeight) {
    var lines = [];
    var inImageData = ctxIn.getImageData(0, 0, width, height);

    /**
     * @param {Number} y
     * @param {Number} [p=0.002]
     */
    function initLine(y, p) {
      var tmpL = new Line(width, p || 0.002);
      tmpL.path.forEach((v, i) => tmpL.path[i] = y);
      return tmpL;
    }

    function initCanvas() {
      dCtx.fillStyle = 'rgba(0, 0, 0, 0.7)';
      dCtx.strokeStyle = 'rgba(255, 255, 255, 1)';
      dCtx.fillRect(0, 0, dWidth, dHeight);
      dCtx.fillStyle = 'rgba(0, 0, 0, 1)';
    }

    /**
     * @param {Number} [helpLinesCount=1]
     */
    function initLines(helpLinesCount) {
      var ind;
      var lastP = null;
      var topAvg = 0, bottomAvg = 0;
      var specialAvgDivider = Math.ceil(height / 2);
      var hCount = helpLinesCount + 1 || 2; // helpLinesCount + 1
      var tmpL;
      var tmpP;
      for (var y = 0; y < height; y++) {
        var sum = 0;
        for (var x = 0; x < width; x++) {
          ind = (x + (y * width)) * 4;
          sum += inImageData.data[ind] + inImageData.data[ind + 1] + inImageData.data[ind + 2];
        }
        var avg = sum / (width * 3 * 255);
        // avg *= 0.8; // thinner lines than must be!

        if (lastP !== null) {
          for (var hLine = 1; hLine < hCount; hLine++) {
            tmpP = (avg * hLine + lastP * (hCount - hLine)) / hCount;
            tmpL = initLine(y - 1 + hLine / hCount, tmpP / hCount);
            lines.push(tmpL);
          }
        }

        if (y - specialAvgDivider < 0) {
          topAvg += avg;
        }
        if (y + specialAvgDivider >= height) {
          bottomAvg += avg;
        }

        lastP = avg;
        tmpL = initLine(y, avg / hCount);
        lines.push(tmpL);
      }
      // We need more lines. This lead to full covered image.
      topAvg /= specialAvgDivider;
      bottomAvg /= specialAvgDivider;
      for(var l = 0; l < specialAvgDivider * hCount; l++) {
        tmpP = (topAvg * l) / (hCount * hCount * specialAvgDivider);
        tmpL = initLine(0 - specialAvgDivider + l / hCount, tmpP);
        lines.push(tmpL);
        tmpP = (bottomAvg * l) / (hCount * hCount * specialAvgDivider);
        tmpL = initLine(height + specialAvgDivider - l / hCount, tmpP);
        lines.push(tmpL);
      }
    }

    function drawLines() {
      for (var i = 0; i < lines.length; i++) {
        lines[i].draw(dCtx, dWidth);
      }
    }

    function pixelMatrix() {
      var matrix = [];
      var ind;
      for (var x = 0; x < width; x++) {
        matrix[x] = matrix[x] || [];
        for (var y = 0; y < height; y++) {
          ind = (x + (y * width)) * 4;
          var pixelNorm = (inImageData.data[ind] + inImageData.data[ind + 1] + inImageData.data[ind + 2]) / (3 * 255);
          matrix[x][y] = pixelNorm;
        }
      }

      return matrix;
    }

    /**
     * @param {Number} factor - 1 like normal average, 0.1 - very light smoothing
     */
    function smooth(factor) {
      var l;
      for (l = 0; l < lines.length; l++) {
        for (var x = 0; x < width; x++) {
          var lft = lines[l].path[x - 1] || lines[l].path[x];
          var rht = lines[l].path[x + 1] || lines[l].path[x];
          lines[l].path[x] = (lines[l].path[x] + lft * factor + rht * factor) / (2 * factor + 1);
        }
      }
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @param {Number} pixelSize - normal - 0.5, more than 0.5 - bright image, less than 0.5 - dark image
     * @return {{see: number, lines: Array}}
     */
    function whatISee(x, y, pixelSize) {
      var l;
      var see = 0;
      var seeLines = [];
      // What we see?
      for (l = 0; l < lines.length; l++) {
        var seeTmp = lines[l].path[x] - y;
        var dy = (lines[l].path[x] - lines[l].path[x - 1]) || (lines[l].path[x + 1] - lines[l].path[x]) || 0;
        var weightMul = Math.sqrt(dy * dy + 1);
        var seeTmpBottom = seeTmp + lines[l].p / 2;
        var seeTmpTop = seeTmp - lines[l].p / 2;
        if (seeTmpBottom < 0 && seeTmpBottom > -pixelSize) {
          // -------  - path[x]
          //       ^
          // - - - | = p / 2
          // +++++++  - y
          see += (seeTmpBottom + pixelSize) * weightMul;
          seeLines.push(l);
        } else if (seeTmp > -pixelSize && seeTmp < pixelSize) {
          // - - - -
          //       ^
          // ------|  - path[x] = p
          // ++++++|  - y
          // - - - -
          see += lines[l].p * weightMul;
          seeLines.push(l);
        } else if (seeTmpTop > 0 && seeTmpTop < pixelSize) {
          // +++++++  - y
          // - - - - = p / 2
          //       ^
          // ------|  - path[x]
          see += (pixelSize - seeTmpTop) * weightMul;
          seeLines.push(l);
        }
      }
      return {
        see: see,
        lines: seeLines
      };
    }

    /**
     * @param {Number[][]} matrix
     * @param {Number} shiftOn
     * @param {Number} fromMiddle
     * @param {Number} detectFrom
     */
    function iterateOrdinate(matrix, shiftOn, fromMiddle, detectFrom) {
      var seeMatrix = [];
      seeMatrix.length = width;
      var l;
      for (var x = 0; x < width; x++) {
        seeMatrix[x] = seeMatrix[x] || [];
        matrix[x] = matrix[x] || [];
        var top = Math.floor(height / 2) - fromMiddle;
        var bottom = Math.ceil(height / 2) + fromMiddle;

        bottom = Math.min(bottom, height - 1);
        top = Math.max(top, 0);

        for (var y = top; y <= bottom; y++) {
          var see = whatISee(x, y, 0.5).see; // 0.55 => lines will not glued together
          seeMatrix[x][y] = see;
          // Difference between: we need to see and see
          var diffAbs = Math.abs(matrix[x][y] - see);
          if (diffAbs < detectFrom) {
            continue;
          }
          if (y <= height / 2) {
            // shift all lines above
            if (matrix[x][y] > see) {
              // closer
              for (l = 0; l < lines.length; l++) {
                if (lines[l].path[x] < y) {
                  lines[l].path[x] += shiftOn * diffAbs;
                }
              }
            } else {
              // further
              for (l = 0; l < lines.length; l++) {
                if (lines[l].path[x] < y) {
                  lines[l].path[x] -= shiftOn * diffAbs;
                }
              }
            }
          } else {
            // shift all lines below
            if (matrix[x][y] > see) {
              // closer
              for (l = 0; l < lines.length; l++) {
                if (lines[l].path[x] > y) {
                  lines[l].path[x] -= shiftOn * diffAbs;
                }
              }
            } else {
              // further
              for (l = 0; l < lines.length; l++) {
                if (lines[l].path[x] > y) {
                  lines[l].path[x] += shiftOn * diffAbs;
                }
              }
            }
          }
        }
      }

      return seeMatrix;
    }

    function fitOrdinate() {
      var pixels = pixelMatrix();

      // iterateOrdinate(pixels, 0.5, width, 0.2);
      // smooth(1);
      // iterateOrdinate(pixels, 0.5, width, 0.1);
      // smooth(1);
      // iterateOrdinate(pixels, 0.5, width, 0.05);
      // smooth(1);
      // iterateOrdinate(pixels, 0.25, width, 0.05);
      // smooth(0.5);

      // iterateOrdinate(pixels, 0.5, width, 0.2);
      // smooth(1);
      // iterateOrdinate(pixels, 0.5, width, 0.5);
      // smooth(1);
      // iterateOrdinate(pixels, 0.5, width, 0.7);
      // smooth(1);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      iterateOrdinate(pixels, 0.15, width, 0.1);
      smooth(1.0);
      var testData = iterateOrdinate(pixels, 0.001, width, 1);
      // smooth(1);
      return testData;
    }

    initLines(0);
    initCanvas();
    var testData = fitOrdinate();
    drawLines();
    return testData;
  }

  window.linify = linify;
})();
