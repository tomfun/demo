'use strict';

/**
 * @param {Number} width
 * @param {Number} [p=1]
 * @param {Number[]} [path]
 * @constructor
 */
function Line(width, p, path) {
  this.width = width;
  this.p = p || 1;
  path ? this.setPath(path) : this.resetPath();
}
/**
 * @param {Number[]} path
 * @methodOf Line
 */
Line.prototype.setPath = function setPath(path) {
  if (path.length !== this.width) {
    throw new Error('Wrong path length');
  }
  this.path = path;
};

/**
 * @methodOf Line
 */
Line.prototype.resetPath = function resetPath() {
  this.path = [];
  for (var i = 0; i < this.width; i++) {
    this.path.push(0);
  }
};

/**
 * @param {Number} [scaleX=1]
 * @param {Number} [scaleY=1]
 * @return {Array<Number[]>}
 * @methodOf Line
 */
Line.prototype.toCubicBezier = function toCubicBezier(scaleX, scaleY) {
  var shagX = scaleX || 1;
  var zoomY = scaleY || shagX;
  var x = 0;
  var y = this.path[0] * zoomY;
  var x1 = 0;
  var y1 = 0;
  var x2 = 0;
  var y2 = 0;
  var k = 0.33;
  var result = [];
  for (var i = 0; i < this.width; i++) {
    x1 = x;
    y1 = y;
    y = this.path[i] * zoomY;
    y1 = (1 - k) * y1 + k * y;
    x = i * shagX;
    x2 = k * x1 + (1 - k) * x;
    x1 = (1 - k) * x1 + k * x;
    y2 = y - (+(this.path[i + 1] * zoomY || y) - y) * k;

    result.push([x1, y1, x2, y2, x, y]);
  }
  return result;
};

/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {Number} outWidth
 * @param {Number} [zoomY]
 * @methodOf Line
 */
Line.prototype.draw = function draw(ctx, outWidth, zoomY) {
  if (!(ctx instanceof CanvasRenderingContext2D)) {
    throw new TypeError('ctx must be instance of CanvasRenderingContext2D');
  }
  var bezierData = this.toCubicBezier(outWidth / (this.width - 1), zoomY);
  var prevLineWidth = ctx.lineWidth;
  ctx.beginPath();
  ctx.moveTo(0, bezierData[0][5]);
  ctx.lineWidth = outWidth * this.p / this.width;
  for (var i = 0; i < bezierData.length; i++) {
    ctx.bezierCurveTo.apply(ctx, bezierData[i]);
  }
  ctx.stroke();
  ctx.lineWidth = prevLineWidth;
};

/**
 * @param {Number} extP
 * @param {Number} delta
 * @methodOf Line
 */
Line.prototype.touchedPath = function draw(extP, delta) {
  // var bezierData = this.toCubicBezier();
  // console.log(bezierData)
  var outPath = [];
  outPath.length = this.width;
  outPath[this.width - 1] = this.path[this.width - 1] + 1;
  for (var i = 0; i < this.width; i++) {
    var dy11 = (this.path[i + 1] - this.path[i] || 0);
    var dy12 = (this.path[i] - this.path[i - 1] || 0);
    var dy1 = (dy11 + dy12) / 2;
    var dy2 = (this.path[i] - this.path[i - 1]) || dy11 || dy12;
    var add1 = Math.sqrt(dy1 * dy1 + 1) * (this.p + delta);
    var add2 = Math.sqrt(dy2 * dy2 + 1) * (extP + delta);
    // var dy31 = bezierData[i][1] - this.path[i];
    // var dy32 = bezierData[i][3] - this.path[i];
    var add3 = 0; //Math.sqrt((dy31 > 0 ? (dy31 * dy31) : 0) + (dy32 > 0 ? (dy32 * dy32) : 0));
    outPath[i] = this.path[i] + (add1 + add2) / 2 + add3 / 3;
  }
  for (i = 2; i < this.width; i++) {
    dy11 = this.path[i - 1] - this.path[i - 2];
    dy12 = this.path[i] - this.path[i - 1];
    if (dy11 > dy12) {
      outPath[i - 2] += dy11 * (extP + this.p) / 2 / 4;
    } else {
      // outPath[i - 1] -= dy12 * (extP + this.p) / 2 / 4;
    }
  }
  return outPath;
};

window.Line = Line;
